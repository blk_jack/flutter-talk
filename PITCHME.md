---
## Flutter
![FLUTTER](images/flutter-logo.png)
---
### How is it??
- Easy setup
- Git repo
- CLI is great, flutter doctor is friend
---
### Challenges
![PROG](images/programmatic.png)
---
### Web components
```
<template>
  <div id="app">
    <img src="./assets/logo.png">
    <h1>{{ msg }}</h1>
  </div>
</template>

<script>
export default {
  data () {
    return {
      msg: 'Hello World'
    }
  }
}
</script>

<style>
body {
  font-family: Helvetica, sans-serif;
}
</style>
```
---
### Architecture for state management
```dart
  int counter = 0;

  static const int INCREASE = 1;
  static const int DECREASE = -1;

  void adjustCounter(int operation){
    setState((){
      counter += operation;
    });
}
```
---
### Which?
![BLOC](images/bloc.png)
---
### BLoC
```dart
class IncrementBloc implements BlocBase {
  int _counter;

  StreamController<int> _counterController = StreamController<int>();
  StreamSink<int> get _inAdd => _counterController.sink;
  Stream<int> get outCounter => _counterController.stream;

  StreamController _actionController = StreamController();
  StreamSink get incrementCounter => _actionController.sink;

  IncrementBloc(){
    _counter = 0;
    _actionController.stream
                     .listen(_handleLogic);
  }

  void dispose(){
    _actionController.close();
    _counterController.close();
  }

  void _handleLogic(data){
    _counter = _counter + 1;
    _inAdd.add(_counter);
  }
}
```
---
### Velocity
![SANIC](images/sanic.png)
---
### Flutter Conference App
![CONF1](images/speakers.jpg)
---
### Flutter Conference App
![CONF2](images/schedule.jpg)
---
### Flutter Conference App
![CONF3](images/about.jpg)
---
### Flutter Conference App
![CONF4](images/speaker.jpg)
---
### Flutter Conference App
![CONF5](images/talk.jpg)
---
### Who cares?
- White labeled app
- Uses static json data file
---
```json
{
  "about": {
    "venue": {
      "name": "MaRS Discovery District",
      "address": "101 College St, Toronto, ON M5G 1L7, Canada",
      "image_path": "images/venue.png"
    },
    "description": "AndroidTO is Canada's largest running Android Conference and has been a part of the global Google DevFest initiative since 2014.\n\nThe 2018 Event takes place on Tuesday, November 6 featuring a wide range of talks on topics such as: Android Development, Machine Learning, Accessibility, Security, working with emerging technologies and more!",
    "twitter": "https://twitter.com/androidTO",
    "website": "http://androidto.com",
    "contact_email": "mailto:example@email.com",
    "link_types": [
      {
        "id": "sponsor",
        "name": "Sponsors"
      },
      {
        "id": "supporter",
        "name": "Supporters"
      }
    ],
    "links": [
      {
        "name": "Symbility Intersect",
        "image_path": "images/sponsors/symbility.png",
        "website": "https://www.symbilitysolutions.com/symbility-intersect/",
        "link_type_id": "sponsor"
      },
      {
        "name": "Google Developers",
        "image_path": "images/sponsors/google-developers.png",
        "website": "https://developers.google.com/",
        "link_type_id": "supporter"
      },
      {
        "name": "MaRS",
        "image_path": "images/sponsors/mars.png",
        "website": "https://www.marsdd.com",
        "link_type_id": "supporter"
      },
      {
        "name": "GDG Toronto Android",
        "image_path": "images/sponsors/gdg.jpg",
        "website": "https://www.meetup.com/ToAndroidDev/",
        "link_type_id": "supporter"
      },
      {
        "name": "Android Pub Night",
        "image_path": "images/sponsors/apn.jpg",
        "website": "http://www.meetup.com/Android-Pub-Night/",
        "link_type_id": "supporter"
      }
    ]
  },
  "speakers": [
    {
      "id": "mallika",
      "name": "Mallika Potter",
      "bio": "Mallika is an Android developer at Thumbtack where she works on helping people find local skilled professionals for any project they could imagine. She is passionate about accessibility engineering and inclusive design, as she firmly believes technology should connect and be inclusive of everyone.",
      "image_path": "images/speakers/mallika-potter.jpg",
      "company": "Thumbtack",
      "twitter": "https://twitter.com/mallikaandroid",
      "linked_in": "",
      "github": "https://github.com/mallikapotter"
    },
    {
      "id": "kristina",
      "name": "Kristina Balaam",
      "bio": "Kristina is a Security Intelligence Engineer at Lookout where she reverse engineers mobile malware. Prior to Lookout, she worked as an Application Security Engineer at Shopify focusing mostly on Android mobile security. Kristina graduated with a Bachelor of Computer Science from McGill University in 2012, and is currently pursuing a MSc. in Information Security Engineering from the SANS Institute of Technology. She blogs about computer security on Instagram and Youtube under the handle @chmodxx.",
      "image_path": "images/speakers/kristina-balaam.jpg",
      "company": "Lookout",
      "twitter": "https://twitter.com/chmodxx_",
      "linked_in": "https://www.linkedin.com/in/kebalaam",
      "github": ""
    },
    {
      "id": "adam",
      "name": "Adam Erb",
      "bio": "Adam Erb is a JVM and Android developer at Square. He grew up in North-Western Ontario. He has worked as a professional programmer for the last 7 years in Norway and Toronto. If you buy him a beer he'll tell you all about it.",
      "image_path": "images/speakers/adam-erb.jpg",
      "company": "Square",
      "twitter": "https://twitter.com/erbal",
      "linked_in": "https://ca.linkedin.com/in/adamerb",
      "github": "https://github.com/aerb"
    },
    {
      "id": "alice",
      "name": "Alice Yuan",
      "bio": "Alice is currently at Patreon as a Senior Android Engineer. She previously led the Core UI team at Pinterest on Android, building high impact core features and reusable UI components. She also worked on performance optimizations, and rearchitecting the android codebase in MVP paradigm. Outside of the Android world, she is passionate about volunteering to teach kids to code, dance, drawing, and traveling around the world.",
      "image_path": "images/speakers/alice-yuan.jpg",
      "company": "Patreon",
      "twitter": "https://twitter.com/names_alice",
      "linked_in": "https://www.linkedin.com/in/aliceyuan",
      "github": "https://github.com/AliceYuan"
    },
    {
      "id": "benoit",
      "name": "Benoît Quenaudon",
      "bio": "Benoît is an Android developer at Square. He focuses on providing the best Android experience possible, both to customers and fellow developers.",
      "image_path": "images/speakers/benoit-quenaudon.jpg",
      "company": "Square",
      "twitter": "https://twitter.com/oldergod",
      "linked_in": "https://ca.linkedin.com/in/bquenaudon",
      "github": "https://github.com/oldergod"
    },
    {
      "id": "sam",
      "name": "Sam Wolfand",
      "bio": "Sam is an Android Engineer at Slack, where he's a founding member of the new user experience team, in his spare time he enjoys trying every burrito in a 20 mile radius.",
      "image_path": "images/speakers/sam-wolfand.jpg",
      "company": "Slack",
      "twitter": "https://twitter.com/swolfand",
      "linked_in": "https://www.linkedin.com/in/samwolfand",
      "github": "https://github.com/swolfand"
    },
    {
      "id": "enrique",
      "name": "Enrique López Mañas",
      "bio": "Enrique López Mañas is a Google Developer Expert and independent IT consultant. He has been working with mobile technologies and learning from them since 2007. He is an avid contributor to the open source community and a FLOSS (Free Libre Open Source Software) kind of guy, being among the top 10 open source Java contributors in Germany. He is a part of the Google LaunchPad accelerator, where he participates in Google global initiatives to influence hundreds of the best startups from all around the globe. He is also a big data and machine learning aficionado.\n\nIn his free time he rides his bike, take pictures, and travels until exhaustion. He also writes literature and enjoys all kinds of arts. He likes to write about himself in third person.",
      "image_path": "images/speakers/enrique-lopez-manas.jpg",
      "company": "Google Developer Expert",
      "twitter": "https://twitter.com/eenriquelopez",
      "linked_in": "https://www.linkedin.com/in/eenriquelopez",
      "github": "https://github.com/kikoso"
    },
    {
      "id": "francisco",
      "name": "Francisco Estévez García",
      "bio": "A former Android developer, I now work on development tools and infrastructure at Facebook UK. During the cold London nights I contribute to the Kotlin community to enhance ecosystem with battle-tested constructs from other languages, to help you focus on deliverables!",
      "image_path": "images/speakers/francisco-estevez-garcia.jpg",
      "company": "Facebook UK",
      "twitter": "",
      "linked_in": "",
      "github": ""
    },
    {
      "id": "alex",
      "name": "Alex Saveau",
      "bio": "Alex is a passionate open source Android developer. He loves to build apps with Firebase and contribute to numerous libraries. Notably, he's the co-author of FirebaseUI Android, Gradle Play Publisher, Easy Permissions, and LicenseAdapter. Whenever something is broken or can be improved, he prefers to go ahead fix it while learning how the technology works rather than add to the maintainers’ backlog.\n\nHe also loves participating in alpha and beta tests to experience the bleeding edge of unreleased software and play with something new.\n\nWhen he isn’t slowly losing his eyesight in front of a computer screen, he likes to go on hikes and camping or bikepacking trips out in nature to find fresh air and photo opportunities.",
      "image_path": "images/speakers/alex-saveau.jpg",
      "company": "Android Developer",
      "twitter": "https://twitter.com/supercilex",
      "linked_in": "https://www.linkedin.com/in/alex-saveau-684972111",
      "github": "https://github.com/SUPERCILEX"
    },
    {
      "id": "yun",
      "name": "Yun Cheng",
      "bio": "Yun Cheng is a software engineer for a mobile development team at ASICS Digital in Boston, Massachusetts. She is currently focusing on performance and stability improvements in the Android Runkeeper app. She also volunteers as a facilitator for the Girls Who Code club in Cambridge, MA.",
      "image_path": "images/speakers/yun-cheng.jpg",
      "company": "ASICS Digital",
      "twitter": "https://twitter.com/yuncheng13",
      "linked_in": "https://www.linkedin.com/in/yun-cheng-76772b2b",
      "github": ""
    },
    {
      "id": "eric",
      "name": "Eric Fung",
      "bio": "Since 2010, Eric has worked on many mobile apps and games, and spent five years at Shopify as an Android developer, before recently transitioning to a data scientist role. At the beginning of his career, he spent a lot of time in Linux and the command-line.\n\nEric caught the public speaking bug a few years ago, and is an organizer of GDG Toronto Android. In addition to coding, he enjoys making and eating elaborate desserts.",
      "image_path": "images/speakers/eric-fung.jpg",
      "company": "Shopify",
      "twitter": "https://twitter.com/gnufmuffin",
      "linked_in": "https://ca.linkedin.com/in/eric-fung-1a856882",
      "github": "https://github.com/efung"
    },
    {
      "id": "simon",
      "name": "Simon Reggiani",
      "bio": "After working as a Senior Android Developer for companies like Texture and Slack, Simon is now dividing his time between his role of VP of Mobile at Poparide and consulting for companies like Bungalow where he is currently building a React Native app.",
      "image_path": "images/speakers/simon-reggiani.jpg",
      "company": "Poparide",
      "twitter": "https://twitter.com/sregg",
      "linked_in": "https://www.linkedin.com/pub/simon-reggiani",
      "github": "https://github.com/sregg"
    },
    {
      "id": "nick",
      "name": "Nick Felker",
      "bio": "Nick is a developer programs engineer working on the Google Assistant, Smart Home, Assistant SDK, Android Things, and everything in-between. He graduated from Rowan University last year with a bachelor's degree in Electrical & Computer Engineering.",
      "image_path": "images/speakers/nick-felker.jpg",
      "company": "Google",
      "twitter": "https://twitter.com/handnf",
      "linked_in": "https://www.linkedin.com/in/nickfelker",
      "github": "https://github.com/Fleker"
    },
    {
      "id": "kyri",
      "name": "Kyri Paterson",
      "bio": "Kyri is a Backend Software Engineer at Symbility Intersect where she works on a variety of projects from enterprise banking software to video-mixing apps. Kyri graduated from Systems Design Engineering at the University of Waterloo, and has been teaching Machine Learning in both professional and casual settings ever since. She has a special interest in product design and engineering ethics that carries through her work and style of teaching.",
      "image_path": "images/speakers/kyri-paterson.jpg",
      "company": "Symbility Intersect",
      "twitter": "",
      "linked_in": "",
      "github": ""
    }
  ],
  "tracks": [
    {
      "id": "main",
      "name": "Main",
      "color": "#F0BD35"
    },
    {
      "id": "track1",
      "name": "Track 1",
      "color": "#CC6666"
    },
    {
      "id": "track2",
      "name": "Track 2",
      "color": "#B5BD68"
    }
  ],
  "talk_types": [
    {
      "id": "technical",
      "name": "Technical",
      "material_icon": "build",
      "description": "This talk is considered technical"
    },
    {
      "id": "room",
      "name": "Whole Room",
      "material_icon": "all_inclusive",
      "description": "This talk can be enjoyed by anyone"
    },
    {
      "id": "security",
      "name": "Security",
      "material_icon": "security",
      "description": "This talk is focused on security"
    },
    {
      "id": "fun",
      "name": "Fun",
      "material_icon": "tag_faces",
      "description": "This talk is for fun"
    },
    {
      "id": "break",
      "name": "Break",
      "material_icon": "free_breakfast",
      "description": "Time for a break!"
    },
    {
      "id": "registration",
      "name": "Registration",
      "material_icon": "android",
      "description": "Welcome to the party pal"
    },
    {
      "id": "lunch",
      "name": "Lunch",
      "material_icon": "restaurant",
      "description": "Time for lunch"
    },
    {
      "id": "party",
      "name": "Party",
      "material_icon": "cake",
      "description": "After party"
    }
  ],
  "schedule": [
    {
      "time": "8:00am",
      "talks": [
        {
          "talk_type_id": "registration",
          "title": "Doors Open - Registration",
          "description": "Welcome to AndroidTO 2018!  Pick up your badge and shirt!"
        }
      ]
    },
    {
      "time": "8:55am",
      "talks": [
        {
          "track_id": "main",
          "talk_type_id": "room",
          "title": "Opening Remarks"
        }
      ]
    },
    {
      "time": "9:00am",
      "talks": [
        {
          "speaker_id": "sam",
          "track_id": "main",
          "talk_type_id": "room",
          "title": "Building a Delightful (Enterprise Grade) Login Experience",
          "description": "This talk is about the 1.5 year multiple iteration effort of creating a better login experience for our users. it highlights both technical and product challenges and builds a narrative of the detail and craftsmanship that we put into each product we ship."
        }
      ]
    },
    {
      "time": "9:35am",
      "talks": [
        {
          "speaker_id": "mallika",
          "track_id": "main",
          "talk_type_id": "room",
          "title": "Accessibility @ Scale ",
          "description": "Accessibility often isn’t a priority for companies as they grow, but if you don’t invest in it early it can become a knotty problem. In this talk Mallika will share how to break down a complex Android application to find low-hanging fixes as well as prioritizing larger improvements and investing in accessibility in the future. She will explore her experience growing grassroots support for accessibility improvements at companies and both the moral and business value of improving it."
        }
      ]
    },
    {
      "time": "10:30am",
      "talks": [
        {
          "speaker_id": "eric",
          "track_id": "main",
          "talk_type_id": "room",
          "title": "How The Command-Line Can Give You Superpowers",
          "description": "I want to show you how the macOS command-line can make quick work of many things Android developers need to do. Learning how to use command-line tools will give you more ways to get your job done, and get it done faster.\n\nI'll demonstrate real-world techniques for how you can type less in the terminal, search your projects with ease, manipulate images and JSON files, automate deployment and testing, and more. All without needing to point, click, or swipe!"
        }
      ]
    },
    {
      "time": "11:10am",
      "talks": [
        {
          "speaker_id": "kristina",
          "track_id": "main",
          "talk_type_id": "security",
          "title": "Auditing Your APKs Like a Black Hat Hacker",
          "description": "One of the best ways to guard against exploitation is to analyze your application in the same style as a black hat hacker. Data leaks, insecure storage and a slew of other vulnerabilities are all identifiable simply by reverse-engineering an APK. In this talk, we’ll walk through how to get started with auditing your own applications: reversing the APK, analyzing the reversed source & finding areas that could be vulnerable to attack. We’ll also cover tools and shortcuts for automating some of these tasks."
        }
      ]
    },
    {
      "time": "12:00pm",
      "talks": [
        {
          "talk_type_id": "lunch",
          "title": "Lunch",
          "description": "Eat some food. Have a drink."
        }
      ]
    },
    {
      "time": "1:00pm",
      "talks": [
        {
          "speaker_id": "alex",
          "track_id": "track1",
          "talk_type_id": "room",
          "title": "Dynamic App Modules: Building For the Next Billion ",
          "description": "Have you been thinking about modularizing your app, but haven't found the incentive to do so yet? That will change in this session where you will learn to use dynamic app modules to split your monolith into logical features—each of which can be downloaded independently, thus saving users’ data. Learn tips and tricks on building, testing, and deploying your dynamic features. By the end of the session, you will be familiar with the structure of a dynamic app, know how to communicate between the different feature modules, and be able to effectively use the internal track to test them."
        },
        {
          "speaker_id": "adam",
          "track_id": "track2",
          "talk_type_id": "technical",
          "title": "Less is More",
          "description": "Constraint Layouts, Fragments, Data Binding, View Models, Architecture Components. The continuous release of new abstractions and paradigms can be a draining process for even the most experienced programmers. For this talk I focus on the Zen like process of leaving it all behind, and striping away layers, rather than adding them. Android has always provided all the tools you need, you just need to know where to look for them. I talk about the Canvas and how you can draw fast and complex views, the Android Layout System and how you can implement custom responsive layouts, and using the Touch Event System to create smooth gestures across view boundaries. This talk is applicable for every level of Android developer. Familiarity with Java is the only prerequisite."
        }
      ]
    },
    {
      "time": "1:35pm",
      "talks": [
        {
          "speaker_id": "simon",
          "track_id": "track1",
          "talk_type_id": "room",
          "title": "React Native: A Beginner's Journey",
          "description": "React Native and other cross-platform solutions are getting more and more popular. As Android developers, we can either be scared of them or embrace them. In this talk, I'll share my journey from the former to the latter, and what I learnt from it."
        },
        {
          "speaker_id": "benoit",
          "track_id": "track2",
          "talk_type_id": "technical",
          "title": "Applying Rx Best Practices to Your Architecture",
          "description": "Your relationship with RxJava doesn't have to be complicated. We find many ways to use it wrong; it is also powerful when used properly. In fact, RxJava can guide you in shaping a sound architecture for your app. We only have to follow a few but decisive principles.\n\nIn this talk, we'll:\n\n- See at how side-effect isolation can help avoiding bugs.\n- Learn how to share one unique stream between your view and your presenter.\n- Discover the ways a unidirectional data flow makes adding new functionality easy and safe.\n- Look at how data immutability brings safety to data manipulation.\n\nAfter this talk, you'll be able to write a robust and reactive architecture for your app, taking full advantage of RxJava."
        }
      ]
    },
    {
      "time": "2:10pm",
      "talks": [
        {
          "speaker_id": "yun",
          "track_id": "track1",
          "talk_type_id": "room",
          "title": "Adventures of an Android Developer in iOS Land",
          "description": "What happens when a seasoned Android developer finds herself having to learn iOS from the beginning over the course of a few months? This survival guide will discuss lessons learned, differences in developing in the two platforms, and tips and resources to help you transition from Android to iOS development."
        },
        {
          "speaker_id": "enrique",
          "track_id": "track2",
          "talk_type_id": "technical",
          "title": "Kotlin/Native for Multiplatform Development",
          "description": "Reliable multiplatform development has been the Holy Grail for Mobile Developers since its inception. Many frameworks did appear and disappear. Kotlin/Native has come strongly, and since it is still in Beta version there are some caveats about this. Is Kotlin/Native reliable for my app? Is it production-ready?\n\nCome to this talk to learn what Kotlin/Native can do for you."
        }
      ]
    },
    {
      "time": "2:45pm",
      "talks": [
        {
          "speaker_id": "nick",
          "track_id": "track1",
          "talk_type_id": "room",
          "title": "Building Conversational Experiences with Actions on Google",
          "description": "Actions on Google is a platform to allow developers to build conversational actions across a range of device modalities and form factors including Android phones and smart displays. In this talk you'll learn how to get started, and how extend your services to reach new users in new ways."
        },
        {
          "speaker_id": "francisco",
          "track_id": "track2",
          "talk_type_id": "room",
          "title": "Simple Dependency Management in Kotlin ",
          "description": "In this talk we will discover how we can use just Kotlin features to inject dependencies through our codebase. Using this novel approach we'll introduce the concept of Type Classes, a unit of abstraction and configuration used to architect apps and libraries."
        }
      ]
    },
    {
      "time": "3:35pm",
      "talks": [
        {
          "talk_type_id": "break",
          "title": "Break"
        }
      ]
    },
    {
      "time": "3:55pm",
      "talks": [
        {
          "speaker_id": "kyri",
          "track_id": "main",
          "talk_type_id": "room",
          "title": "Machine Learning: Beyond the Tutorial",
          "description": "If it's not Blockchain, Machine Learning may be the buzzword you've been hearing most these days in the tech world. Maybe you've even gotten your hands on some introductory tutorials on the topic. In this talk, Kyri is going to take you Beyond the Tutorial into the world of production code, planning, pitfalls, and big ideas in the Machine Learning space. She will explore the most important things to know and remember the tutorials never tell you, what Machine Learning looks like in an increasingly mobile world, and where this all applies in real everyday life."
        }
      ]
    },
    {
      "time": "4:30pm",
      "talks": [
        {
          "speaker_id": "alice",
          "track_id": "main",
          "talk_type_id": "room",
          "title": "Common Poor Coding Patterns and How to Avoid Them",
          "description": "Every engineer has great intentions when diving into a codebase. You want to make the code beautiful, easy to read, and modular. However as time passes and new feature and quick bug fixes are put in, somehow your android codebase turned into a mess of multiple levels of inheritance of views and fragments and you're stuck wondering how you ended up here in the first place.\n\nIn my talk I will share with you common poor patterns that every developer first makes when developing in android and the alternative architecture patterns to ensuring clean code. We will talk about listeners, event driven paradigms and clean architecture."
        }
      ]
    },
    {
      "time": "5:15pm",
      "talks": [
        {
          "talk_type_id": "room",
          "title": "Flutter",
          "description": "TBD"
        }
      ]
    },
    {
      "time": "5:45pm",
      "talks": [
        {
          "track_id": "main",
          "title": "Closing Remarks",
          "description": "The end of the show.."
        }
      ]
    },
    {
      "time": "6:00pm",
      "talks": [
        {
          "talk_type_id": "party",
          "title": "After Party",
          "description": "It's the party that happens after!"
        }
      ]
    }
  ]
}
```
